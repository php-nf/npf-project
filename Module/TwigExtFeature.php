<?php

namespace Module;

use Npf\Core\Common;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig_SimpleFunction;

/**
 * Class TwigExtFeature
 * Enhanced curl and make more easy to use
 */
class TwigExtFeature extends AbstractExtension
{
    private $lang = '';

    private $languages = [
        'zhHans' => '中文',
        'zhHant' => '中文繁體',
        'en' => 'English',
    ];

    private $discData = [];

    /**
     * @return array|TwigFilter
     */
    final public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('strCount', 'substr_count'),
        ];
    }

    /**
     * @return array|TwigFilter
     */
    final public function getFilters()
    {
        return [
            new TwigFilter('i18n', [$this, 'translation']),
            new TwigFilter('getVarType', [$this, 'getVarType']),
            new TwigFilter('lineCount', [$this, 'lineCount']),
        ];
    }

    /**
     * @param $text
     * @return string
     */
    final public function translation($text)
    {
        return isset($this->discData[$text]) ? (string)$this->discData[$text] : $text;
    }

    /**
     * @param $content
     * @return string
     */
    final public function lineCount($content)
    {
        $lines = explode("\n", str_replace(["\r\n", "\r"], "\n", $content));
        return count($lines);
    }

    /**
     * @param $lang
     * @param string $defaultLang
     * @return mixed
     */
    final public function setLang($lang, $defaultLang = 'en')
    {
        $lang = str_replace("-", "", $lang);
        if (!isset($this->languages[$lang]))
            $lang = $defaultLang;
        $dictClass = "Module\\I18n\\{$lang}";
        if (class_exists($dictClass)) {
            $dict = new $dictClass();
            $this->discData = $dict();
            $this->lang = $lang;
        }
        return $lang;
    }

    /**
     * @param $data
     * @return string
     */
    final public function getVarType($data)
    {
        $result = gettype($data);
        if ($result === 'array' && Common::isAssocArray($data))
            $result = 'assoc';
        return $result;
    }

    /**
     * @param $data
     * @return string
     */
    final public function hello($data)
    {
        return 'hello';
    }
}